*** Settings ***
Library    SeleniumLibrary
Library     CryptoLibrary    variable_decryption=True
*** Comments ***
Este arquivo de testes automatiza o login no SERH

*** Variables ***
${BROWSER}    Chrome
# Variáveis abaixo devem ser setadas no arquivo .env
${URL}        %{URL_LOGIN_SIP_SERH}
${LOGIN}      %{LOGIN_SERH}
${SENHA}      %{SENHA_SERH}
${ORGAO}      %{ORGAO_SERH}

*** Keywords ***
Preencher Dados Login
  [Arguments]    ${usuario}    ${senha}    ${orgao}
  Input Text      id:txtUsuario    ${usuario}
  Input Text      id:pwdSenha   ${senha}
  Select From List By Label    id:selOrgao      ${orgao}

*** Test Cases ***
Login no site
    Open Browser    ${URL}    ${BROWSER}
    Maximize Browser Window
    Preencher Dados Login    ${LOGIN}    ${SENHA}    ${ORGAO}
    Click Button    id:sbmAcessar
    Wait Until Element Is Visible    id:divInfraBarraLocalizacao
