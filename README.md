# Login SERH com Robot/Selenium

**Fazendo login no SERH via Robot/Selenium com variáveis de ambiente!**

Este guia irá ajudá-lo a configurar e executar seu RobotFramework em um ambiente isolado via pipenv, sem precisar compartilhar seus dados particulares

### Requisitos

* Python 3.8
* Pip
* pipenv

### Verificando se o pipenv está instalado

Abra um terminal e digite o seguinte comando:

```shell
pipenv --version
``` 

Se o Pipenv estiver instalado, você verá a versão do Pipenv instalada, como no exemplo abaixo
```
pipenv, version 2023.12.1
```
#### Caso não esteja instalado, siga o guia abaixo

### Instalando o Pipenv
Se o Pipenv não estiver instalado, você pode instalá-lo usando o Pip:

```shell 
python -m pip install pipenv
```

Agora já poderá usar o pipenv para criar ambientes virtuais

### Instalando as dependências do projeto

Abra o terminal e navegue até a pasta raiz do seu projeto. Execute o seguinte comando:

```shell 
pipenv install
```

Este comando irá instalar as dependências do seu projeto, conforme especificado no arquivo `Pipfile`.


### Inserindo as variáveis de ambiente

Renomeie o arquivo `copia.env` na raiz do seu projeto para apenas `.env`\
Insira os valores para seu login e senha particulares\
Em `LOGIN_SERH`, insira seu login\
Em `URL_LOGIN_SIP_SERH`, insira a url de login para o sistema desejado\
Em `ORGAO_SERH`, insira a label do orgão desejado\
Siga o exemplo abaixo para referência

**Exemplo:**
```
URL_LOGIN_SIP_SERH=https://urldoserh
LOGIN_SERH=usu99
ORGAO_SERH=TRF4
```

Verifique que nada está em `SENHA_SERH`, pois isso será definido abaixo

### Gravando senha de forma segura

Para preencher a variável `SENHA_SERH` em `.env`, precisamos criptografá-la de forma que o Robot Framework reconheça corretamente\
Para isso, devemos entrar no shell do pipenv

```shell 
pipenv shell
```

O Pipenv Shell é um ambiente virtual isolado que contém as dependências do seu projeto. 

Visto que já executamos  `pipenv shell`, podemos começar a criar a criptografia

Execute o comando

```shell 
CryptoLibrary
```

Você verá na tela o seguinte menu, onde deve escolher a opção **Open Config**:
```commandline
? What do you want to do? (Use arrow keys)
   Encrypt
   Decrypt
 » Open config
   Quit
```

Escolha a opção **Configure key pair**

```commandline
? What do you want to do? (Use arrow keys)
 » Configure key pair
   Configure public key
   Back
```

Escolha a opção **Configure key pair**

```commandline
? What do you want to do? (Use arrow keys)
 » Generate key pair
   Set key path
   Set key pair from string
   Delete key pair
   Save private key password
   Delete saved password
   Back
```
Escolha a opção **Yes**
```commandline
? Do you want to regenerate the key pair? (Use arrow keys)
 » Yes
   No
```

Escolha a opção **Yes**
```commandline
? Do you want save password? (Use arrow keys)
 » Yes
   No
```
Isso irá gerar um par de chaves criptografadas, que devem ser guardadas com uma senha de sua escolha
* ### Não use sua senha de login como senha para o par de chaves criadas

Sua chave publica será mostrada na tela, com um menu logo após

Escolha a opção **Back** duas vezes até voltar ao primeiro menu, onde deve escolher **Encrypt**

```commandline
? What do you want to do? (Use arrow keys)
 » Encrypt
   Decrypt
   Open config
   Quit
```

Agora você deve digitar sua senha de login.\
Quando pressionar Enter, receberá o hash de sua senha. Algo como no exemplo abaixo

```crypt:+senhaEncriptada=```

Copie a senha que aparece em seu terminal e escolha a opção **Quit** para encerrar o programa

### Inserindo o hash da senha como variável de ambiente

Cole a o hash da senha no arquivo `.env` em `SENHA_SERH` 

Para que a senha seja salva como variável de ambiente, deve sair do shell do pipenv e voltar novamente

```shell
exit

pipenv shell
```
Agora estará rodando o shell dentro do ambiente isolado de seu projeto, apenas com as variáveis de ambiente setadas em seu arquivo `.env` e com as dependências especificadas em seu `Pipfile`

### Executando o seu código

No Pipenv Shell, você pode executar o seu código Python usando o seguinte comando:

`python ./main.py`

Caso preferir, pode rodar diretamente o arquivo `teste.robot` que já está na raiz do projeto, configurado para usar as variáveis de ambiente no arquivo `.env`

`robot ./teste.robot`

